import React, { useEffect, useContext, useState } from "react";
import PropTypes from "prop-types";
import styled, { keyframes, css } from "styled-components";
import IosArrowForward from "react-ionicons/lib/IosArrowForward";

var tinycolor = require("tinycolor2");

const WHITE_COLOR_STRENGTH = 50;
const DARK_COLOR_STRENGTH = 10;

let textColor = "#ccc";
let hoverColor = "#ccc";

const textFocus = keyframes`
  0% {
    opacity: 0.5;
  }
  100% {
    opacity: 1;
  }
`;

const StyledInput = styled.input`
  border-width: 2px;
  background-color: ${props => props.color};
  padding: 7px;
  font-size: 17px;
  text-align: left;
  border-style: hidden;
  border-radius: 13px;
  ${props =>
    props.width &&
    props.height &&
    css`
        width: ${props.width};
        height: ${props.height};
    }`}
  ${props =>
    props.focus &&
    css`
      &:focus{
        animation: ${textFocus} 0.4s forwards;
      }
    }`}
`;

const IconWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: center;
`;

const getReverseColor = (color, reveseStrength) => {
  const backgroundColor = tinycolor(color);
  if (backgroundColor.isDark()) {
    return backgroundColor.brighten(reveseStrength).toString();
  }
  return backgroundColor.darken(reveseStrength).toString();
};

const renderIcon = icon => {
  return React.cloneElement(icon, { color: textColor, size: 10 });
};
const getButtonChildren = (label, icon, iconPosition = "left") => {
  if (label && icon) {
    const iconElement = renderIcon(icon);
    return (
      <IconWrapper>
        {iconPosition === "left" ? iconElement : null}
        {label}
        {iconPosition === "right" ? iconElement : null}
      </IconWrapper>
    );
  }
  return label;
};

/**
 * Button component
 *
 * @param   {string}    className styled-components className for custom styling
 * @returns {component}           React component
 */

const TextInput = ({ size, background, label, icon, iconPosition, theme }) => {
  const color = theme === "light" ? "#000" : "#fff";
  const strength =
    theme === "light" ? WHITE_COLOR_STRENGTH : DARK_COLOR_STRENGTH;
  const backgroundColor = getReverseColor(color, strength);
  const [isHovered, setIsHovered] = useState(false);
  return (
    <>
      <StyledInput
        background={background}
        color={backgroundColor}
        hoverColor={hoverColor}
        isHovered={isHovered}
        onMouseOver={() => setIsHovered(true)}
        onMouseOut={() => setIsHovered(false)}
      />
    </>
  );
};

TextInput.propTypes = {
  background: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.elementType,
  iconPosition: PropTypes.oneOf(["left", "right"]),
  theme: PropTypes.oneOf(["light", "dark"])
};

TextInput.defaultProps = {
  background: "yellow",
  label: "primary",
  theme: "light"
};

export default TextInput;
